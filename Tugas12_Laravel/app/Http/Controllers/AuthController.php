<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function daftar(){
        return view("pages.register");
    }

    public function data(Request $request){
        $namaDepan = $request['fname'];
        $namaBelakang = $request['lname'];
        $jenisKelamin = $request['jk'];
        $kebangsaan = $request['kb'];
        $alamat = $request['address'];

        return view("pages.welcome",["namaDepan"=>$namaDepan,"namaBelakang"=>$namaBelakang,"jenisKelamin"=>$jenisKelamin,"kebangsaan"=>$kebangsaan,"alamat"=>$alamat]);
    }
}
