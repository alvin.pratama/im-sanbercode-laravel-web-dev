<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<body>
    <h1>Buat Account Baru</h1>
    <h2>Sign Up Form</h2>

    <form action="/signup" method="POST">
        @csrf
        <label for="">First Name:</label><br>
        <input type="text" name="fname"><br><br>
        <label for="">Last Name:</label><br>
        <input type="text" name="lname"><br><br>
        <label for="">Gender:</label><br>
        <input type="radio" name="jk" value="Laki-Laki"> Man <br>
        <input type="radio" name="jk" value="Perempuan"> Woman <br>
        <input type="radio" name="jk" value="Lainnya"> Other <br><br>
        <label for="">Nationality: </label>
        <select name="kb">
            <option value="Indonesia">Indonesia</option>
            <option value="Jepang">Jepang</option>
            <option value="Arab">Arab</option>
            <option value="Rusia">Rusia</option>
          </select>
          <br><br>
        <label for="">Alamat:</label><br>
        <textarea name="address" id="" cols="30" rows="10"></textarea><br><br>
        <button type="submit">Sign Up</button>     
    </form>
</body>
</html>