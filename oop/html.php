<?php

require("animal.php");
require_once("ape.php");
require_once("frog.php");

$sheep = new Animal("shaun");
echo "Name: " . $sheep->name . "<br>"; // "shaun"
echo "Legs: " . $sheep->legs . "<br>"; // 4
echo "Cold blooded: " . $sheep->cold_blooded . "<br>"; // "no"

$sungokong = new Ape("kera sakti");
echo "<br>Name: " . $sungokong->name . "";
echo "<br>Legs: " . $sungokong->legs . "";
echo "<br>Cold blooded: " . $sungokong->cold_blooded . "";
$sungokong->yell();

$kodok = new Frog("buduk");
echo "<br><br>Name: " . $kodok->name . "";
echo "<br>Legs: " . $kodok->legs . "";
echo "<br>Cold blooded: " . $kodok->cold_blooded . "";
$kodok->jump() ; // "hop hop"